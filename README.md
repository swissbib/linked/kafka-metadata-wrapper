# Kafka Metadata Wrapper

Provides a simple model for transmitting structured metadata along with
a Kafka message. Includes also a Serdes serializer and deserializer for
the model.

## Installation

### Gradle

```groovy
repositories {
  maven {url 'http://gitlab.com/api/v4/projects/11507450/packages/maven'}
}

dependencies {
    compile 'org.swissbib:kafka-metadata-wrapper:1.1.0'
}
```


## Usage

Use the serializer / deserializer by declaring 

* `org.swissbib.SbMetadataSerializer` or
* `org.swissbib.SbMetadataDeserializer`

in the respective Kafka properties. E.g.

```java
public class TestConsumer {
    public static void main(String[] args) {
        Properties kafkaProps = new Properties();
        kafkaProps.put("key.deserializer",
            "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
            "org.swissbib.SbMetadataDeserialize");
        // ...
    }
}
```

### Provided fields

* `data`: Message data (as String)
* `status`: Status of data (as String array)
* `category`: Data category (as String array)
* `version`: Message version (as Long)

### Release

To create a new release add a tag on the latest commit and push to the master branch.
This will create a new release with that tag. The tag must be in the from x.y.z and must be
higher in at least one position than the previous tag. To comply with semantic versioning
the x (major) version number should be increased if there is a breaking change. The y (minor)
version number should be increased when there is a new feature implemented in a backwards
compatible way. The z (patch) version number should be increased when a bug is fixed.