package org.swissbib.types;

public enum EsBulkActions {
    INDEX, DELETE, UPDATE, CREATE
}
