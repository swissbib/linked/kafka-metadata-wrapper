package org.swissbib.types;

public enum CbsActions {
    CREATE, REPLACE, DELETE
}
