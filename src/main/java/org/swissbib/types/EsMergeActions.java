package org.swissbib.types;

public enum EsMergeActions {
    NEW, RETRY, UPDATE
}
