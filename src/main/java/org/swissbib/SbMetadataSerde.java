package org.swissbib;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class SbMetadataSerde implements Serde<SbMetadataModel> {
    private SbMetadataSerializer serializer = new SbMetadataSerializer();
    private SbMetadataDeserializer deserializer = new SbMetadataDeserializer();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<SbMetadataModel> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<SbMetadataModel> deserializer() {
        return deserializer;
    }
}
