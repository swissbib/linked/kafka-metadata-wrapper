/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.swissbib;

import org.apache.kafka.common.errors.SerializationException;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SbMetadataModelTest {
    private final String encoding = "UTF8";
    private SbMetadataModel sourceModel;
    private SbMetadataModel sinkModel;

    @Before
    public void setup() {
        sourceModel = new SbMetadataModel();
        sinkModel = new SbMetadataModel();
    }

    @Test
    public void serializeEmptyIndexName() {
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assertNull("Sink model should contain no category", sinkModel.getEsIndexName());
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }

    @Test
    public void serializeIndexName() {
        String fruit = "Bananas";
        sourceModel.setEsIndexName(fruit);
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assertEquals("Sink model should only contain category '" + fruit + "'",
                    fruit, sinkModel.getEsIndexName());
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }

    @Test
    public void serializeSingleCategoryWithSpecialCharacters() {
        String specialFruit = "Pine" + SbMetadataModel.listElementSeparator + "apple";
        sourceModel.setEsIndexName(specialFruit);
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assertEquals("Sink model should only contain category '" + specialFruit + "'",
                    specialFruit, sinkModel.getEsIndexName());
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }

/*    @Test
    public void serializeMultipleCategories() {
        String[] fruits = {"Apple", "Orange", "Pear"};
        sourceModel.setCategory(fruits);
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assertArrayEquals("Sink model should contain categories " + Arrays.toString(fruits),
                    fruits, sinkModel.getCategory().toArray());
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }

    @Test
    public void serializeMultipleSpecialFields() {
        String[] status = {"enabled", "dis" + SbMetadataModel.fieldSeparator + "abled"};
        String status2 = "between";
        String[] finalStatus = {"enabled", "dis" + SbMetadataModel.fieldSeparator + "abled", "between"};
        String json = "{\"location\":\"no" + SbMetadataModel.keyValueSeparator + "where\", \"weight:\":999.9, \"nulled\":null}";
        sourceModel.setStatus(status);
        sourceModel.setStatus(status2);
        sourceModel.setData(json);
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assertArrayEquals("Sink model should contain status " + Arrays.toString(finalStatus),
                    finalStatus, sinkModel.getStatus().toArray());
            assertEquals("Sink model should contain data '" + json + "'",
                    json, sinkModel.getData());
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }*/

    @Test
    public void serializeVersionField() {
        long version = 12L;
        sourceModel.setEsDocumentPrimaryTerm(version).setEsDocumentSeqNum(1024L);
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assert sourceModel.getEsDocumentPrimaryTerm() == sinkModel.getEsDocumentPrimaryTerm();
            assert sourceModel.getEsDocumentSeqNum() == sinkModel.getEsDocumentSeqNum();

        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }


    @Test
    public void serializeNullVersionField() {
        try {
            byte[] asByteArray = sourceModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assert !sinkModel.useEsOptimisticConcurrencyControl();
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }

    @Test(expected = NullPointerException.class)
    public void getNullVersion() {
        sourceModel.getEsDocumentPrimaryTerm();
    }


    @Test(expected = UnsupportedEncodingException.class)
    public void throwUnsupportedEncodingExceptionOnWrongEncoding() throws UnsupportedEncodingException {
        String encoding = "UTF4";
        byte[] asByteArray = sourceModel.toByteArray(encoding);
        sinkModel.fromByteArray(asByteArray, encoding);
    }

    @Test
    public void serializeMessageDateField() {
        String date = "2019-08-10";
        sinkModel.setMessageDate(date);
        try {
            byte[] asByteArray = sinkModel.toByteArray(encoding);
            sinkModel.fromByteArray(asByteArray, encoding);
            assert sinkModel.getMessageDate().equals(date);
        } catch (UnsupportedEncodingException e) {
            throw new SerializationException("Error when (de-)serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }
}
